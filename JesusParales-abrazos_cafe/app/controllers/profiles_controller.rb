class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :current_user

  def index
    @profiles = Profile.last
    @ni = @profiles.name.chars
   
  end

  def show
  end

  def new
    @user = User.find current_user.id
    @profile = @user.build_profile
  end

  def create
    @user = User.find current_user.id
    @profile = @user.build_profile(profile_params.except("multiplier"))
    @profile.multiplier = profile_params[:multiplier].join
    @profile.save
  end

  def update
  end

  def edit
  end

  def delete
  end

  private 

  def profile_params 
    params.require(:profile).permit(:name,:ocupation,:bio,{multiplier:[]})

  end 



end
