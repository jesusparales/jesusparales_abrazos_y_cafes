require 'test_helper'

class MugsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get mugs_index_url
    assert_response :success
  end

  test "should get show" do
    get mugs_show_url
    assert_response :success
  end

  test "should get create" do
    get mugs_create_url
    assert_response :success
  end

  test "should get update" do
    get mugs_update_url
    assert_response :success
  end

  test "should get delete" do
    get mugs_delete_url
    assert_response :success
  end

end
