class CreateMugs < ActiveRecord::Migration[5.2]
  def change
    create_table :mugs do |t|
      t.integer :quantity
      t.decimal :final_price
      t.references :profile

      t.timestamps
    end
  end
end
