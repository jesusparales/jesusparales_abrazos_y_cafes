class CreateHugs < ActiveRecord::Migration[5.2]
  def change
    create_table :hugs do |t|
      t.decimal :time
      t.references :profile

      t.timestamps
    end
  end
end
