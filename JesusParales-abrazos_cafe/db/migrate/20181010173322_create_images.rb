class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.string :subtitle
      t.integer :public
      t.references :profile

      t.timestamps
    end
  end
end
