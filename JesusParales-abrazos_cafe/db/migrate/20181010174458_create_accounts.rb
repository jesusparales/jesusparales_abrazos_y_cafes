class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :entity
      t.integer :kind
      t.integer :number
      t.references :profile

      t.timestamps
    end
  end
end
